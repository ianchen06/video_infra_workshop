#!/bin/sh
wget http://ftp.gnu.org/gnu/glibc/glibc-2.14.tar.gz
tar -zxvf glibc-*
cd glibc-2.14
mkdir build
cd build
sudo mkdir /opt/glibc-2.14
../configure --prefix=/opt/glibc-2.14
sudo make
sudo make install
