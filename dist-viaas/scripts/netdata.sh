#!/bin/sh
sudo yum -y install zlib-devel gcc make git autoconf  automake pkgconfig
git clone https://github.com/firehol/netdata.git --depth=1
cd netdata
sudo ./netdata-installer.sh
