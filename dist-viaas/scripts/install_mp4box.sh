#!/bin/sh
git clone https://github.com/gpac/gpac
cd gpac
git checkout 97eaffd
./configure
make all
sudo make install-lib install
echo "/usr/local/lib" >> /etc/ld.so.conf.d/gpac.conf
sudo /sbin/ldconfig
