# VIaaS Ansible playbook

Ansible playbook for video infrastructure deployment

## Getting Started

###Requirements###

1. Python 2.7.10
2. Ansible
3. SSH access to servers with root and "{{remote_user}}" access. (refer vars/main.yml)
4. ansible-galaxy install goozbach.EPEL geerlingguy.firewall geerlingguy.glusterfs geerlingguy.ntp

## Deployment

0. It assume you have configured 'producer' with it's IP in your /etc/hosts file.(workers too!)
1. Edit the inventory file for running playbooks, it is located in playbook directory.
2. Edit the `celery` in playbooks for target hosts. Refer to step 1.
3. Configure variables in `vars/main.yml` (Such as: remote_user, gluster mount dir)
4. Run `ansible-playbook your-playbook.yml`

## Description

**00-site.yml** - installs all the required packages and python & it's required packages.

**01-site-admin.yml** - builds infrastructure

**02-site-repoCopy.yml** - clones repository

**03-site-nginx.yml** - install & configure nginx virtual hosts




## Operation

####Celery####

**Restart** `ansible-playbook -i inventory 04-site-manage.yml`