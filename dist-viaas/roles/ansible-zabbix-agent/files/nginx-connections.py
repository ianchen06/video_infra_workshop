import requests
import socket
import sys

hostname = socket.gethostname()
url = 'http://'+hostname+'/status/format/json'
requests = requests.get(url)
result = requests.json()


def returner():
    if sys.argv[1] == 'active':
        return result['connections']['active']
    elif sys.argv[1] == 'reading':
        return result['connections']['reading']
    elif sys.argv[1] == 'writing':
        return result['connections']['writing']
    elif sys.argv[1] == 'waiting':
        return result['connections']['waiting']
    elif sys.argv[1] == 'accepted':
        return result['connections']['accepted']
    elif sys.argv[1] == 'handled':
        return result['connections']['handled']
    elif sys.argv[1] == 'requests':
        return result['connections']['requests']

if __name__ == "__main__":
        print returner()
