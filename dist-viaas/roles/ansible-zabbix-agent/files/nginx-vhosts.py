import requests
import socket
import sys

hostname = socket.gethostname()
url = 'http://'+hostname+'/status/format/json'
requests = requests.get(url)
result = requests.json()


def returner():
    vhost = sys.argv[1]
    if sys.argv[2] == 'requestCounter':
        return result['serverZones'][vhost]['requestCounter']
    elif sys.argv[2] == 'inBytes':
        return result['serverZones'][vhost]['inBytes']
    elif sys.argv[2] == 'outBytes':
        return result['serverZones'][vhost]['outBytes']
    elif sys.argv[2] == '1xx':
        return result['serverZones'][vhost]['responses']['1xx']
    elif sys.argv[2] == '2xx':
        return result['serverZones'][vhost]['responses']['2xx']
    elif sys.argv[2] == '3xx':
        return result['serverZones'][vhost]['responses']['3xx']
    elif sys.argv[2] == '4xx':
        return result['serverZones'][vhost]['responses']['4xx']
    elif sys.argv[2] == '5xx':
        return result['serverZones'][vhost]['responses']['5xx']

if __name__ == "__main__":
        print returner()
