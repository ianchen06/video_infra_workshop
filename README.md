## Scalable Video Infrastructure Workshop

### Getting Started

1. Install [vagrant](https://www.vagrantup.com)
2. Download box file for this workshop
3. `vagrant up`
4. `ansible-playbook -h host dist-viaas/00-site.yml --tags vendor`
