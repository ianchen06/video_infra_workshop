from boxsdk import OAuth2, Client
import json
from bin.tasks.authenticate import authenticate
from pprint import pprint
from datetime import datetime
import time
import sys

epoch_time = int(time.time())

VIDEO_TYPE_LIST = ['tv_adult', 'tv_video', 'video', 'tv_adult_test']

def search_files(client,line):
        search_results = client.search(line, limit=5, offset=0)
        myfile = open("{}_foundIDs.txt".format(epoch_time), "a")
        for item in search_results:
            pprint(vars(item))
            video_type = item.path_collection['entries'][1]['name']
            print(video_type)
            print(item.parent['name'])
            if video_type in VIDEO_TYPE_LIST:
                print("it is in the correct folder")
                item_with_name = item.get(fields=['name'])
                print('matching item for: ' + line + ' and id is: ' + item_with_name.id)
                myfile.write("'" + item_with_name.id + "'" + ',')
                if video_type == 'video':
                    print('fixing apen')
                    video_type = 'apen'
                if video_type == 'tv_adult_test':
                    print('fixing tv adult test')
                    video_type = 'tv_adult'
                return item_with_name.id, video_type
            else:
                print("Not in this result, continue")
        else:
            print('no matching item for: ' + line)
            with open("not_foundFiles.txt", "a") as myfile:
                myfile.write(line + '\n')

def main():
    oauth = authenticate('search')
    client = Client(oauth)
    me = client.user(user_id='me').get()
    print('user_login: ' + me['login'])
    lines = ['avid56fa62a25fc3b', 'avid56fa62a25fc42', 'avid56fa62a25fca8']
    for line in lines:
        search_files(client,line)

    #with open(sys.argv[1]) as f:
    #    lines = [line.rstrip('\n') for line in f]

if __name__ == '__main__':
    main()

