#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

This script fixes apen video cover with a input file of avkeys

"""
import os
import subprocess as sp

output_dir = '/'
with open('fix_list.txt') as f:
    for line in f:
        line = line.strip()
        if not os.path.isdir('/{}/{}'.format(output_dir, line)):
            sp.call(['mkdir', '/{}/{}'.format(output_dir, line),])
        print line
        command = [
            'php',
            './php_scripts/crawl_img.php',
            line,
            '/{}/{}/{}-b.jpg'.format(output_dir, line, line)
        ]
        sp.call(command)
        command2 = [
            'php',
            './php_scripts/thumimages/thumimages.php',
            # REFACTOR cover image path
            output_dir + line + '/' + line + '-b.jpg',
            line,
            output_dir,
        ]
        sp.call(command2)
