#!/bin/bash
#
# This is the reference bash scirpt for stopping all running celery instances
#
#
LOG_DIR=/tmp/run/celery/
cd /tmp/run/celery/
for filename in ${LOG_DIR}/*.pid; do
  kill -9 `cat ${filename}`
done
rm -rf ${LOG_DIR}/*
