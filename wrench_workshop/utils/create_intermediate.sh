#!/bin/bash
RAW_FILE=$1
INTERMEDIATE_MP4=$2

echo input raw file is ${RAW_FILE}
echo output file is ${INTERMEDIATE_MP4}


ffmpeg \
  -y \
  -i \
  /${RAW_FILE} \
  -movflags +faststart \
  -c:v libx264 \
  -r 24\
  -g 24 \
  -keyint_min 24 \
  -sc_threshold 0 \
  -b:v 750k \
  -maxrate 750k \
  -bufsize 1500k \
  -vf scale=-2:576 \
  /${INTERMEDIATE_MP4}
