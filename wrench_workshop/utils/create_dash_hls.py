#!/usr/bin/env python
import os
from bin.tasks import transcode

cnt = 0
with open('too_small_todo.list') as avkeys:
    for avkey in avkeys:
        avkey = avkey.split('/')[-1].strip()
        #avkey = avkey.split('\t')[0].strip()
        print avkey
        #if cnt > 299:
        #    print avkey
        transcode.get_cover.apply_async((avkey, '/mnt/strea/apen/'), queue='create_dash_{}'.format(os.environ['HOSTNAME']))
        transcode.create_hls.apply_async(('/tmp/download/apen/{}.mp4'.format(avkey), '/bricks/apen/', avkey), queue='create_hls_{}'.format(os.environ['HOSTNAME']))
        #transcode.create_dash.apply_async(('/tmp/download/apen/{}.mp4'.format(avkey), '/bricks/apen/', avkey, '/dev/shm/'), queue='create_dash_{}'.format(os.environ['HOSTNAME']))
        #cnt = cnt + 1

        transcode.create_dash_tv.apply_async(({
            'intermediate_mp4': '',
            'tmp_dir': '',
            'filename': '',
            'stream_dir': '',
            }), queue=''

        transcode.create_hls_tv.apply_async(({
            'intermediate_mp4': '',
            'filename': '',
            'stream_dir': '',
            }), queue=''

        transcode.get_video_thumbnail_process(
                {
                    'stream_dir': '',
                    'filename': '',
                    }
                )
