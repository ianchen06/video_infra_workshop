#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

This script mimics the tree command, walks the current directory recursively
prints out the files and directories and writes out to a text

"""
import os
import shutil


# traverse root directory, and list directories as dirs and files as files
f = open('missing_file.txt', 'w')
for root, dirs, files in os.walk("."):
    path = root.split('/')
    print (len(path) - 1) *'---' , os.path.basename(root)       
    cnt = 0
    for file in files:
        cnt = cnt + 1
        print len(path)*'---', file
    print (len(path) + 1) *'---' , os.path.basename(root), cnt
    if cnt < 5:
        if len(os.path.basename(root)) > 1:
            f.write(os.path.basename(root) + '\t' + str(cnt) + '\n')
            print 'removing', os.path.basename(root)
            #shutil.rmtree(os.path.basename(root))
            #os.rmdir(os.path.basename(root))
    
