#!/bin/bash
#
# This bash script deletes all Rabbitmq queues regardless of whether it is empty or not
# install rabbitmqadmin first with wget http://localhost:15672/cli/rabbitmqadmin
# chmod +x rabbitmqadmin and move it to your path
#

rabbitmqadmin --vhost=vhost --username=vagrant --password=vagrant -f tsv -q list queues name | while read queue; do rabbitmqadmin --vhost=vhost --username=vagrant --password=vagrant -q delete queue name=${queue}; done
