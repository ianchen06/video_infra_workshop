#!/bin/bash
INDEX_FILENAME=$1
SEGMENT_URL_TEMPLATE=$2
SEGMENT_FILENAME_TEMPLATE=$3
OUTPUT_FILE=$4

echo index_filename is $1
echo segment_url_template is $2
echo segment_filename_template is $3
echo output_file is $4

mp42hls \
--hls-version 3 \
--index-filename $INDEX_FILENAME \
--segment-duration 5 \
--segment-url-template $SEGMENT_URL_TEMPLATE \
--segment-filename-template $SEGMENT_FILENAME_TEMPLATE \
$OUTPUT_FILE
