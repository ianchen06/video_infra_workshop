![](http://i.imgur.com/O4XFgaI.png)
#CK Works (Formerly wrench)
-

**TL;DR** 

Ingest any video of any format/encoding, it will output nicely transcoded x264 files in the following formats

1. MP4 
2. MPEG-DASH 
3. HLS

ready for HTTP streaming with fronend HTML5 player like [video.js](https://github.com/videojs/videojs-contrib-hls)

Table of Contents
-
1. [What is CK Works](#id-section1)
2. [Architecture](#id-section2)
3. [Installation](#id-section3)
4. [Getting Started](#id-section4)
5. [Project Structure](#id-section5)
6. [Celery Walkthrough](#id-section6)
7. [Task Routing](#id-section7)
8. [FFMPEG and HTTP Streaming](#id-section8)




What is CK Works?
-

CK Works is a video processing pipeline framework for supporting large scale video content services. It provides a complete backend that it is dramatically simpler to ingest and get the desired output video formats. CK Works is extensible, integrates with the Python stack.

CK Works is built on top of Celery and consists of the following components:

* Video ingester -- a python script that utilizes the Box.net python sdk and listens for changes in the desired folders on Box.net.
* Celery tasks -- a python celery project utilizing the chain() method for a pipelined workflow, more details below.

**note:** This repository does not install the necessary binaries, such as RabbitMQ, ffmpeg..etc. Please see [dist_viaas](https://bitbucket.org/appimc_video/dist-viaas) for installing them via Ansible.

Architecture
-

![](docs/architecture.png)

Installation
-

Clone the repository

```
$ git clone git@bitbucket.org:appimc_video/wrench_v2.git
$ cd wrench_v2
$ pip install -r requirements.txt
```

Getting Started
-
Prerequisites
-

Please install them via [dist_viaas](https://bitbucket.org/appimc_video/dist-viaas)

1. RabbitMQ server
2. Python 2.7.9, with pip
3. FFMPEG
4. Bento4 Toolbox

Worker node
-

1. Look at `environments` and make changes to the environment variables accordingly to your `dev.env` and `prod.env` files. For example, your RabbitMQ address and credentials and Box.net credentials
2. Soures the corresponding environment files, for example:
`$ source environments/dev.env`
3. Obtain box api key and refresh token and place in `/tmp/key_celery`, please refer to the [box sdk documentation](https://developers.box.com/sdks/) on how to obtain it.
4. `$ ./start_celery.sh dev` to start dev environment and `$ ./start_celery.sh prod` for production

Controller node
-

1. Same steps 1 and step 2 as the worker node.
2. Obtain box api key and refresh token and place in `/tmp/key_controller` and another pair of keys as `/tmp/key_search`, please refer to the [box sdk documentation](https://developers.box.com/sdks/) on how to obtain it.
3. Start the controller with `$ python controller.py`
4. Start flower management console with `$ celery flower --address=0.0.0.0 --port=5555`
5. Start the flask api for submitting repair jobs with `$ python flask_app.py`

Project Structure
-

```
app
bin
 |- tasks
    |- auto_onshelf    // Contains legacy php scripts for modifying the apen database
    |- common.py       // Contains common tasks
    |- transcode.py    // Contains video transcoding tasks
    |- authenticate.py // Contains box authentication tasks
docs
environments
 |- prod.env           // Production environment
 |- dev.env            // Dev environment
utils //Contains useful scripts for one-off jobs
 |- create_dash_hls.py // Sample script for executing batch dash and hls creation jobs
 |- create_hls_live.sh // Creates hls video files
 |- create_intermediate.sh // FFMPEG execution script with specs
 |- find_by_name.py    // Sample scirpt for finding box file_ids with the file_name by using box search api
 |- get_cover.py       // Sample script for batch fixing video covers
 |- rabbitmq_delete_queues.sh // Sample script for purging and deleteing all queues on local rabbitmq server DANGEROUS
 |- walk.py            // Recursively traversing the directory, useful for finding broken videos in a haystack
php_script
```

Celery Walkthrough
-

1. ./start_celery.sh
	- Dynamic worker configuration management. 
2. celeryconfig.py
   - Task routing definitions.
3. task.py
	- Utilized python's subprocess.Popen() for calling other system processes.
	- System commands are stored separately as shell scripts.
	- stdout, stderr = pipe.communicate() to synchronously wait for process result.
	- Function arguments of each subprocess are passed along the chain as a dictionary.


Task Routing
-

There are two kinds of routing conerns in this project

1. Side effect aware subprocesses
2. Long running vs short running tasks

### Side effect aware subprocess

For performance optimizations, we want to minimize the number of times writing to distributed storage or network access during the video process lifetime.


Processes that generates side effects that requires a disk write but is not our final object, are routed to a queue locally, and side effects are written to local disk, preferrably a SSD based storage.

Common tasks which does not have large production of side effects, can be routed back to a common queue and allow the tasks to be distributed to available workers.

Example: download, transcode, create\_hls, create\_dash

### Long running vs short running tasks

It is generally best practice to separate tasks based on their estimated execution time, to avoid blocking.

Generally long videos(hours longs) and short videos(minutes) should be routed to different queues.

Example: transcode\_short\_vid, transcode\_long\_vid


Transcode and HTTP Streaming
-

There are two stages of media processing,

1. FFMPEG transcoding to H264
2. Media fragmentation and packaging to HTTP streaming format

### FFMPEG transcoding

With references from the official [FFMPEG document](https://trac.ffmpeg.org/wiki/EncodingForStreamingSites) as well as the [Google live video guide line](https://support.google.com/youtube/answer/2853702) the reference configuration for ffmpeg is 

```
ffmpeg \
  -y \
  -i \
  /${RAW_FILE} \
  -movflags +faststart \
  -crf 21 \
  -bf 2 \
  -flags +cgop \
  -pix_fmt yuv420p \
  -c:v libx264 \
  -r 24\
  -g 24 \
  -keyint_min 24 \
  -sc_threshold 0 \
  -b:v 750k \
  -maxrate 750k \
  -bufsize 1500k \
  -vf scale=-2:576 \
  /${INTERMEDIATE_MP4}
```

Additional References:

1. [https://kvssoft.wordpress.com/2015/01/28/mpeg-dash-gop/](https://kvssoft.wordpress.com/2015/01/28/mpeg-dash-gop/)

NOTE: The + sign indicates that ffmpeg should set the specified value in addition to any values that the MOV/MP4 muxer will automatically set during the course of executing the command. Omitting it means ffmpeg will reset the flags to their default values, and only toggle the state of faststart. Most MP4s generation doesn't involve the other flags so usually it doesn't make a difference

### Create HTTP Streaming files

Two formats are created in this project

1. HTTP Live Streaming (HLS)
2. MPEG-DASH

Note: The secment duration must be a multiplication of the GOP, in this case we are using a GOP size of 1 sec.

#### HLS

```
mp42hls \
--hls-version 3 \
--index-filename $INDEX_FILENAME \
--segment-duration 5 \
--segment-url-template $SEGMENT_URL_TEMPLATE \
--segment-filename-template $SEGMENT_FILENAME_TEMPLATE \
$OUTPUT_FILE
```

#### MPEG-DASH

```
mp4fragment --fragment-duration 5000 ${input_file} ${output_file}
```

```
'mp4dash',
'-f',
#'--profiles=on-demand',
'--profiles=live',
'--mpd-name={}.mpd'.format(filename),
'--media-prefix={}'.format(filename),
'-o', '/{}/{}/'.format(output_dir, filename),
'/{}'.format(fragmented_mp4),
```



Developemnt
-

#####Note: Please read the [celery documentation](http://docs.celeryproject.org/en/latest/index.html) first. Pay specific attention to *Canvas: Designing workflows* and *Routing Tasks* as this project utilizes these advanced features

1. Develop new task in `bin/tasks/`
2. Configure the routing in `celeryconfig.py` to decide which queue the task should be routed to
3. Configure the `./start_celery.sh` to decide which worker queues should be started and the concurrency configuration

