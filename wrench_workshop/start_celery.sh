#!/bin/bash
PID_DIR=/tmp/run/celery/
LOG_DIR=/tmp/log/celery/

DOWNLOAD_DIR=/tmp/download_dir/
TMP_DIR=/tmp/tmp_dir/

mkdir -p $DOWNLOAD_DIR
mkdir -p $TMP_DIR

usage() {
    echo ""
    echo "  Usage:"
    echo "      `basename $0` [dev||prod|stop]"
    echo ""
    echo "          dev/prod: run celery app in different env mode"
    echo "          stop:  kill celery process when in production mode"
    echo ""
    echo ""
    exit
}

startCelery() {
    #celery worker -E --config=celeryconfig --loglevel=INFO --concurrency=2 --pool=gevent
    celery multi start \
        shared_tasks \
        process_media_${HOSTNAME} \
        process_media_tv_${HOSTNAME} \
        create_dash_apen_${HOSTNAME} \
        create_dash_tv_${HOSTNAME} \
        create_hls_apen_${HOSTNAME} \
        create_hls_tv_${HOSTNAME} \
        create_thumbnail_${HOSTNAME} \
        network_transport_${HOSTNAME} \
        downloader_apen_${HOSTNAME} \
        transcode_apen_${HOSTNAME} \
        downloader_tv_${HOSTNAME} \
        transcode_tv_${HOSTNAME} \
      -E --config=celeryconfig \
      --loglevel=INFO \
      -c:shared_tasks 20 \
      -c:process_media_${HOSTNAME} 20 \
      -c:process_media_tv_${HOSTNAME} 20 \
      -c:create_dash_apen_${HOSTNAME} 3 \
      -c:create_dash_tv_${HOSTNAME} 3 \
      -c:create_hls_apen_${HOSTNAME} 10 \
      -c:create_hls_tv_${HOSTNAME} 10 \
      -c:create_thumbnail_${HOSTNAME} 20 \
      -c:network_transport_${HOSTNAME} 200 \
      -c:downloader_apen_${HOSTNAME} 1 \
      -c:transcode_apen_${HOSTNAME} 1 \
      -c:downloader_tv_${HOSTNAME} 1 \
      -c:transcode_tv_${HOSTNAME} 1 \
      -Q:shared_tasks shared_tasks \
      -Q:process_media_${HOSTNAME} process_media_${HOSTNAME} \
      -Q:process_media_tv_${HOSTNAME} process_media_tv_${HOSTNAME} \
      -Q:create_dash_apen_${HOSTNAME} create_dash_apen_${HOSTNAME} \
      -Q:create_dash_tv_${HOSTNAME} create_dash_tv_${HOSTNAME} \
      -Q:create_hls_apen_${HOSTNAME} create_hls_apen_${HOSTNAME} \
      -Q:create_hls_tv_${HOSTNAME} create_hls_tv_${HOSTNAME} \
      -Q:create_thumbnail_${HOSTNAME} create_thumbnail_${HOSTNAME} \
      -Q:network_transport_${HOSTNAME} network_transport_${HOSTNAME} \
      -Q:downloader_apen_${HOSTNAME} downloader_apen_${HOSTNAME} \
      -Q:transcode_apen_${HOSTNAME} transcode_apen_${HOSTNAME} \
      -Q:downloader_tv_${HOSTNAME} downloader_tv_${HOSTNAME} \
      -Q:transcode_tv_${HOSTNAME} transcode_tv_${HOSTNAME} \
      --pool=gevent \
      --pidfile="/tmp/run/celery/%n.pid" \
      --logfile="/tmp/log/celery/%n.log"
}

check_env() {
    if [ -z ${AMQP_ADDR} ]; then export AMQP_ADDR=$1; else echo "AMQP_ADDR is set to '$AMQP_ADDR'"; fi
}


if [ -z $1 ];
then
    usage
fi

mkdir -p $PID_DIR
mkdir -p $LOG_DIR

case $1 in
    'dev')
        source environments/dev.env
        echo "Starting in dev mode: ${AMQP_ADDR}"
        startCelery
        ;;
    'prod')
        source environments/prod.env
        echo "Starting in production mode: ${AMQP_ADDR}"
        startCelery
        ;;
    'stop')
        for filename in ${PID_DIR}/*.pid; do
          kill -9 `cat ${filename}`
        done
        rm -rf ${PID_DIR}/*
        sleep 3
        ps -ef|grep celery
        ;;
    *)
        echo ""
        echo "  ERROR: not supported argument => [$1]"
        echo ""
        ;;
esac
