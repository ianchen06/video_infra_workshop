#!/usr/bin/env python
"""This script prompts a user to enter a message to encode or decode
using a classic Caeser shift substitution (3 letter shift)"""

import os
import sys
sys.path.insert(0, os.getcwd())

CELERY_SEND_EVENTS = True
CELERY_TASK_PUBLISH_RETRY = True
BROKER_HEARTBEAT = 30
BROKER_CONNECTION_RETRY = True
BROKER_CONNECTION_MAX_RETRIES = 100
BROKER_CONNECTION_TIMEOUT = 4

CELERY_ACKS_LATE = True
CELERYD_PREFETCH_MULTIPLIER = 1

BROKER_URL = os.environ['AMQP_ADDR']

CELERY_IMPORTS = ("bin.tasks.transcode",)


CELERY_TIMEZONE = 'UTC'

CELERY_ROUTES = {
    'bin.tasks.transcode.add_apen_video': {'queue': 'shared_tasks'},
    'bin.tasks.transcode.add_tv_video': {'queue': 'shared_tasks'},
    'bin.tasks.transcode.fixcover': {'queue': 'shared_tasks'},
    'bin.tasks.transcode.get_cover': {'queue': 'shared_tasks'},
    'bin.tasks.transcode.auto_onshelf': {'queue': 'shared_tasks'},
    'bin.tasks.transcode.thumbnail_process': {'queue': 'shared_tasks'},
    'bin.tasks.transcode.get_video_thumbnail_process': {'queue': 'shared_tasks'},
    'bin.tasks.transcode.change_status': {'queue': 'shared_tasks'},
    'bin.tasks.transcode.download_cover': {'queue': 'shared_tasks'},
    'bin.tasks.transcode.process_media': {'queue': 'process_media_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.process_media_tv': {'queue': 'process_media_tv_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.downloader_apen': {'queue': 'downloader_apen_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.transcode_apen': {'queue': 'transcode_apen_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.downloader_tv': {'queue': 'downloader_tv_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.transcode_tv': {'queue': 'transcode_tv_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.create_dash_apen': {'queue': 'create_dash_apen_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.create_dash_tv': {'queue': 'create_dash_tv_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.create_hls_apen': {'queue': 'create_hls_apen_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.create_hls_tv': {'queue': 'create_hls_tv_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.create_thumbnail': {'queue': 'create_thumbnail_{}'.format(os.environ['HOSTNAME'])},
    'bin.tasks.transcode.network_transport': {'queue': 'network_transport_{}'.format(os.environ['HOSTNAME'])},
}

CELERY_CREATE_MISSING_QUEUES = True
CELERY_IGNORE_RESULT = True
