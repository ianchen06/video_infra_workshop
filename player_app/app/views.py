"""Flask view"""
from flask import render_template, request
from app import app
import base64
from hashlib import md5
import calendar
import datetime

def gen_secure_link(video_id, ip):
    secret = "baobaodontsay"
    url = '/localhost:3333/static/{0}/{0}.m3u8'.format(video_id)

    future = datetime.datetime.utcnow() + datetime.timedelta(minutes=5)
    expiry = calendar.timegm(future.timetuple())

    secure_link = "{key}{url}{expiry}".format(key=secret,
                                              url=url,
                                              expiry=expiry)

    hash = md5(secure_link).digest()
    encoded_hash = base64.urlsafe_b64encode(hash).rstrip('=')
    return url + "?st=" + encoded_hash + "&e=" + str(expiry)

@app.route('/')
@app.route('/index/<id>')
def index(id=None):
    """My index view"""
    client_ip = request.remote_addr
    secure_link = gen_secure_link(id, client_ip)
    return render_template('index.html', video_id=id, secure_link=secure_link)
